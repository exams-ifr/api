import mongoose from "mongoose"

export default class Database {
    constructor() {
        this.connect()
    }

    async connect() {
        await mongoose.connect("mongodb://localhost:27017/cadjee-crm").catch(err => console.error(err))
    }
}