import User from "../../models/users/user.js";
import { userValidator } from "../../validators/user/user.js";
import createController from "../../services/controller.js";
import argon2 from 'argon2';

const userModel = new User();

const { insert } = createController(userModel, userValidator);

// On hash le mot de passe
const insertSurcharge = async (req, res) => {
    // On passe en condition pour pas empecher le validator de fonctionné
    if (req.body['password']) {
        req.body['password'] = await argon2.hash(req.body['password'])
    }

    // On vérifie si un utilisateur n'existe pas déj)
    const existingUser = await userModel.getByEmail(req.body);

    if (existingUser) {
        return res.status(400).send({
            message: "Un utilisateur existe déjà"
        });
    }

    // On continue insert classique
    return insert(req, res);
}

export default insertSurcharge;