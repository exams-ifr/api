import User from '../../models/users/user.js'
import argon2 from 'argon2'
import { generateToken } from '../../services/token.js'

const userModel = new User();

const login = async (req, res) => {
    try {
        // On récupère notre modèle
        const userModel = new User()
        // On execute la requête
        const user = await userModel.getByEmail(req.body)
        // Si l'utilisateur n'existe pas
        if (!user) {
            return res.status(404).json({ message: `L'utilisateur avec l'adresse ${req.body['email']} n'existe pas` })
        }
        // On vérifie le mot de passe qui est hashé en argon2
        const verifyPasswordHash = argon2.verify(user.password, req.body['password'])
        // Dans le cas ou le mot de passe n'est pas correct
        if (!verifyPasswordHash) {
            return res.status(401).json({ message: `L'adresse email ou le mot de passe est incorrect` })
        }
        // Si tout va bien, on lui retourne un token
        return res.status(200).json({ token: generateToken(user._id) })
    } catch (error) {
        // En cas de problème de serveur
        return res.status(500).json({ message: error.message })
    }
}

export default login
