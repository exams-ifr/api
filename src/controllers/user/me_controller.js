import User from "../../models/users/user.js";
import createController from "../../services/controller.js";

const { getById } = createController(new User(), {})

const me = async (req, res) => {
    if (!req.user) {
        return res.status(400).send({
            message: "Pas connecté"
        });
    }
    return getById(req, res);
}

export default me;