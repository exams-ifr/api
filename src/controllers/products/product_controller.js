import Product from "../../models/products/product.js"
import createController from "../../services/controller.js";
import { productValidator } from "../../validators/products/product.js";

const controller = createController((new Product()), productValidator);

const productController = {
    ...controller,
    insert: async (req, res) => {
        req.body = req.file ? {
            ...req.body,
            thumbnail: [
                `${req.protocol}://${req.get('host')}/uploads/${req.file.filename}`
            ]
        } : req.body;

        return controller.insert(req, res);
    },
    update: async (req, res) => {
        req.body = req.file ? {
            ...req.body,
            thumbnail: [
                `${req.protocol}://${req.get('host')}/uploads/${req.file.filename}`
            ]
        } : req.body;

        return controller.update(req, res);
    }
}
export default productController;