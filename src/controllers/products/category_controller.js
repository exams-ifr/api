import Category from "../../models/products/category.js";
import createController from "../../services/controller.js";
import { categoryValidator } from "../../validators/products/category.js";

export default createController((new Category()), categoryValidator);