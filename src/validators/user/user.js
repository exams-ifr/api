import Joi from 'joi';

export const userValidator = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().required(),
    role: Joi.array(),
    date: Joi.date(),
});