import Joi from 'joi'

export const productValidator = Joi.object({
    name: Joi.string().required(),
    description: Joi.string(),
    thumbnail: Joi.array(),
    createdAt: Joi.date(),
    updatedAt: Joi.date(),
    category: Joi.string().required()
});