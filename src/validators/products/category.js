import Joi from 'joi'

export const categoryValidator = Joi.object({
    name: Joi.string().required(),
    description: Joi.string(),
    createdAt: Joi.date(),
    updatedAt: Joi.date()
});