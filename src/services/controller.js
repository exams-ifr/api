const createController = (model, validator) => {
    return {
        getAll: async (req, res) => {
            try {
                const obj = await model.getAll();
                return res.status(200).send(obj);
            } catch (e) {
                console.error(e);
                return res.status(500).send({ message: e.message });
            }
        },
        getById: async (req, res) => {
            try {
                const obj = await model.getById({ _id: req.params.id });
                return res.status(200).send(obj)
            } catch (e) {
                console.error(e)
                return res.status(500).send({ message: e.message });
            }
        },
        insert: async (req, res) => {
            try {
                const { error } = validator.validate(req.body);
                if (error) {
                    return res.status(400).send({ errors: error });
                }
                const newObj = await model.insert(req.body);
                if (newObj) {
                    return res.status(201).send(newObj);
                }
                return res.status(400).send({ message: "Impossible d'enregistrer " });
            } catch (e) {
                console.error(e)
                return res.status(500).send({ message: e.message });
            }
        },
        update: async (req, res) => {
            try {
                const updateObj = await model.update({ _id: req.params.id }, req.body);
                if (!updateObj) {
                    res.status(404).json({ message: `La tâche avec ID ${req.params.id} n'existe pas` })
                }
                res.status(204).send(updateObj);
            } catch (e) {
                console.error(e)
                res.status(500).send({ message: e.message });
            }
        },
        remove: async (req, res) => {
            try {
                const removeObj = await model.remove({ _id: req.params.id });
                if (!removeObj) {
                    return res.status(404).json({ message: `La tâche avec ID ${req.params.id} n'existe pas` })
                }
                return res.status(204).send(removeObj);
            } catch (e) {
                console.error(e)
                return res.status(500).send({ message: e.message });
            }
        }
    }
}

export default createController;