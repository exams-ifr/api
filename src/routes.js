import express from 'express';
import product from './controllers/products/product_controller.js';
import category from './controllers/products/category_controller.js';
import registerUser from './controllers/user/register_controller.js';
import me from './controllers/user/me_controller.js';
import login from './controllers/user/login_controller.js';
import authMiddleware from './middleware/auth.js';
import multer from './middleware/upload.js';

const routes = express.Router();

const prefix = "/api";

routes.get('/', (req, res) => {
    res.send({
        'message': 'Hello world'
    })
})

// Produits
routes.get(`${prefix}/products`, authMiddleware, product.getAll);
routes.get(`${prefix}/products/:id`, authMiddleware, product.getById);
routes.post(`${prefix}/products`, authMiddleware, multer, product.insert)
routes.put(`${prefix}/products/:id`, authMiddleware, multer, product.update)
routes.delete(`${prefix}/products/:id`, authMiddleware, product.remove)

// Catégorie de produit
routes.get(`${prefix}/categories_p`, authMiddleware, category.getAll);
routes.get(`${prefix}/categories_p/:id`, authMiddleware, category.getById);
routes.post(`${prefix}/categories_p`, authMiddleware, category.insert);
routes.put(`${prefix}/categories_p/:id`, authMiddleware, category.update);
routes.delete(`${prefix}/categories_p/:id`, authMiddleware, category.remove);

// User
routes.post(`${prefix}/register`, registerUser); // Inscription d'un nouveau utilisateur
routes.get(`${prefix}/me`, authMiddleware, me); // Meo
routes.post(`${prefix}/login`, login); // Auth

export default routes