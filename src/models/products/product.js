import Crud from "../../services/crud.js";
import mongoose from "mongoose";

export default class Product extends Crud {
    constructor() {
        super('product', {
            name: {
                type: String,
                required: true
            },
            description: {
                type: String
            },
            thumbnail: {
                type: Array
            },
            createdAt: {
                type: Date,
                default: Date.now
            },
            updatedAt: {
                type: Date,
                default: Date.now
            },
            category: {
                type: mongoose.Schema.Types.ObjectId,
                required: true
            }
        })
    }

}