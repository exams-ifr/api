import Crud from "../../services/crud.js"

export default class Category extends Crud {
    constructor() {
        super('category_product', {
            name: {
                type: String,
                required: true
            },
            description: {
                type: String,
                required: true
            },
            createdAt: {
                type: Date, default: Date.now
            },
            updatedAt: {
                type: Date, default: Date.now
            }
        })
    }
}