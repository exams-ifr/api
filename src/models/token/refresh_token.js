import Crud from "../../services/crud.js";
import mongoose from "mongoose";

export default class Refresh_token extends Crud {
    constructor() {
        super('refresh_token', {
            refresh_token: {
                type: String,
                required: true
            },
            userId: {
                type: mongoose.Schema.Types.ObjectId,
                required: true
            },
            valid: {
                type: Date, default: Date.now
            },
        })
    }
}