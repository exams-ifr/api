import jwt from "jsonwebtoken"

// Permet de vérifier si l'utilisateur est connecté ou non
const isAdminMiddleware = (req, res, next) => {
    const userRoles = req.user && req.user.roles;
    if (userRoles && userRoles.includes('ROLE_ADMIN')) {
        next();
    } else {
        // L'utilisateur n'a pas le rôle "admin", renvoyez une réponse non autorisée
        return res.status(403).json({ error: 'Pas les permissions requises.' });
    }
}

export default isAdminMiddleware;