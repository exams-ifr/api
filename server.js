import express from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import routes from './src/routes.js';
import path from 'path';
import { fileURLToPath } from 'url';

// Configuration dotenv
dotenv.config();

// Configuration server
const app = express();
const port = 3000;

// Configuration Cors
app.use(cors());

// Configuration JSON
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Configuration router
app.use(routes);

// Accès images
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
app.use('/images', express.static(path.join(__dirname, '/public/uploads')));

// Lancement server
app.listen(port, () => {
    console.log(`listen to ${port}`);
});